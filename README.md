* Deep learning for counting cars insatellite images - an AI applicationto estimate the impact of COVID-19
https://bobetocalo.github.io/pdf/bdexp20.pdf



# R

* Convolutional LSTM for spatial forecasting
https://blogs.rstudio.com/ai/posts/2020-12-17-torch-convlstm/


* Geocomputation with R
https://geocompr.robinlovelace.net/


* Visualising spatial data with tmap
https://github.com/mtennekes/tmap-workshop


* Other Sites on Spatial Data Anaysis using R
https://psfaculty.plantsciences.ucdavis.edu/plant/othersites.htm


* Processing satellite image collections in R with the gdalcubes package
https://www.r-spatial.org/r/2019/07/18/gdalcubes1.html


* Spatial Data Analysis Using Artificial Neural Networks Part 1
https://www.r-bloggers.com/2020/10/spatial-data-analysis-using-artificial-neural-networks-part-1/


* Spatial Data Analysis Using Artificial Neural Networks Part 2
https://www.r-bloggers.com/2020/10/spatial-data-analysis-using-artificial-neural-networks-part-2/


* giscoR package
https://github.com/dieghernan/giscoR


* Visualizing Poverty w/ Satellite Data
https://www.kaggle.com/reubencpereira/visualizing-poverty-w-satellite-data/code


* In-Depth CPE Spatial data Mapping
https://www.kaggle.com/ganeshn88/in-depth-cpe-spatial-data-mapping/code


* EDA African Conflicts with Leaflets
https://www.kaggle.com/ambarish/eda-african-conflicts-with-leaflets


* Fun with Meteorites Landings
https://www.kaggle.com/ambarish/fun-with-meteorites-landings-w-maps



# Python

* Learning about Geospatial Analysis with Python
https://www.packtpub.com/product/learning-geospatial-analysis-with-python-third-edition/9781789959277?utm_source=gislounge&utm_medium=site&utm_campaign=5dollar2020&utm_term=gis-analysts-and-developers
https://account.packtpub.com/getfile/9781789959277/code


* RUS Webinar – Processing Copernicus data in Python using snappy – 23 June 2020
https://rus-copernicus.eu/portal/rus-webinar-processing-copernicus-data-in-python-using-snappy/


* Automating GIS-processes 2019 Course 
https://automating-gis-processes.github.io/site/


* Geospatial Analysis
https://www.kaggle.com/learn/geospatial-analysis


* Geo Data EDA And Feature Engineering
https://www.kaggle.com/tmheo74/geo-data-eda-and-feature-engineering





# ArcGIS

* Deep learning in ArcGIS Pro
https://pro.arcgis.com/es/pro-app/help/analysis/image-analyst/deep-learning-in-arcgis-pro.htm


* ArcGIS API for Python
https://github.com/Esri/arcgis-python-api


# Satellites

* RUS Training. E-Learning courses
https://rus-training.eu/course


* https://rus-training.eu/uploads/media/R01-Webinar-Q&A.pdf
https://rus-training.eu/uploads/media/R01-Webinar-Q&A.pdf
https://www.youtube.com/watch?v=1dAhrc-kw8o&feature=youtu.be


* Machine Learning and Object Detection in Spatial Analysis
https://www.gislounge.com/machine-learning-and-object-detection-in-spatial-analysis/?utm_medium=email&utm_campaign=GISNL-Dec-17-2020&utm_source=YMLP


* Copernicus. CORINE Land Cover 
https://land.copernicus.eu/pan-european/corine-land-cover


# Contests

* Drivendata Open Cities AI Challenge: Segmenting Buildings for Disaster Resilience
https://github.com/drivendataorg/open-cities-ai-challenge


* Computer Vision for Building Damage Assessment
https://xview2.org/
https://github.com/DIUx-xView

* AI for assisting in natural disaster relief efforts: the xView2 competition
https://appsilon.com/ai-for-assisting-in-natural-disaster-relief-efforts-the-xview2-competition/

* Creating xBD: A Dataset for Assessing Building Damage from Satellite Imagery
https://openaccess.thecvf.com/content_CVPRW_2019/papers/cv4gc/Gupta_Creating_xBD_A_Dataset_for_Assessing_Building_Damage_from_Satellite_CVPRW_2019_paper.pdf


# Datasets

* Spacenet 7 Multi-Temporal Urban Development
https://www.kaggle.com/amerii/spacenet-7-multitemporal-urban-development

* Computer Vision for Building Damage Assessment
https://xview2.org/
